const controller = require('../controllers/controllers');

module.exports = function(app) {
  app.route('/')
    .get(controller.rootController);
  app.route('/device/count/:deviceId')
    .get(controller.getDeviceCount);
  app.route('/device/count/:deviceId/:count')
    .put(controller.addDeviceCount)
  app.route('/device/airquality/:deviceId/')
    .get(controller.getDeviceAirquality)
  app.route('/device/airquality/:deviceId/:temp/:humidity/:co2')
    .put(controller.addAirquality)
  app.route('/room/')
    .get(controller.getRooms)
  app.route('/room/')
    .post(controller.registerDevice)
    .delete(controller.deleteDeviceFromRoom)
  app.route('/room/:roomID')
    .get(controller.getDevicesRoom)
  app.route('/room/:id/count')
    .get(controller.getRoomCount)
//    .put(controller.modifyDevice);
//  app.route('/room/count/:roomId')
//    .get(controller.getRoomCount)
}
