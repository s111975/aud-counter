const dbservice = require('../services/dbservice.js');


exports.addDeviceCount = async function(req, res){
  var device = req.params.deviceId;
  var count = req.params.count;

  var response = await dbservice.addDeviceCount(device, count);
    res.status(200).send(JSON.stringify(response));
}

exports.addAirquality = async function(req, res){
  var device = req.params.deviceId;
  var temp = req.params.temp;
  var humidity = req.params.humidity;
  var co2 = req.params.co2;

  var response = await dbservice.addAirquality(device, temp, humidity, co2);
  res.status(200).send(JSON.stringify(response));
}

exports.getDeviceAirquality = async function(req, res){
  var device = req.params.deviceId;

  var response = await dbservice.getDeviceAirquality(device);
  res.status(200).send(JSON.stringify(response));
}

exports.deleteDeviceFromRoom = async function(req, res){
  var device = req.body.deviceID;
  var response = await dbservice.removeDeviceFromRoom(device);
  res.status(200).send(JSON.stringify(response));
}
exports.getDeviceCount = async function(req, res){
  var device = req.params.deviceId;

  var result = await dbservice.getDeviceCount(device);
  res.status(200).send(JSON.stringify(result));
}

exports.getRooms = async function(req, res){

  var result = await dbservice.getRooms();
  res.status(200).send(JSON.stringify(result));
}

exports.getDevicesRoom = async function(req, res){

  var roomID = req.params.roomID;
  var result = await dbservice.getAllRooms(roomID);
  res.status(200).send(JSON.stringify(result));
}

exports.registerDevice = async function(req, res){
  var device = req.body.deviceID;
  var room = req.body.roomID;
  var type = req.body.type;
  var response = await dbservice.registerDeviceToRoom(room, device, type);
  dbservice.addDeviceCount(device, 0);
  res.status(200).send(JSON.stringify(response));
}

exports.getRoomCount = async function(req, res) {
  var roomID = req.params.id;

  var result = await dbservice.getRoomCount(roomID);
  res.status(200).send(JSON.stringify(result));
}


exports.rootController = function(req, res){
  res.statusCode = 200;
  res.redirect("index.html");
}
