const mysql = require('mysql');


var con = mysql.createConnection({
  host     : 'localhost',
  port     : '55863',
  user     : 'azure',
  password : '6#vWHD_$',
  database : 'localdb'
});


exports.registerDevice = async function(deviceId){
  return new Promise ((resolve) => {
    con.query(
      `INSERT INTO devicecount (deviceID) VALUES ('${deviceId}');`,
      function(error, results, fields) {
        resolve({"error":error, "results":results, "fields":fields});
      });

    });
}

exports.registerDeviceToRoom = async function(roomId, deviceId, type){
  return new Promise ((resolve) => {
    con.query(
      `INSERT INTO localdb.roomdevice (roomID, deviceID, deviceType)
      VALUES ('${roomId}', '${deviceId}', '${type}');`,
      function(error, results, fields) {
        resolve({"error":error, "results":results, "fields":fields});
      });

    });
}

exports.alterDeviceCount = async function(device, count){
  return new Promise((resolve) => {

    con.query(
      `UPDATE devicecount
      SET count = count + ${count}
      WHERE deviceID = '${device}';`,
        function(error, results,fields) {
          resolve({"error":error, "results":results, "fields":fields});
        });

  })
}

exports.addDeviceCount = async function(device, count){
  return new Promise((resolve) => {

    con.query(
      `INSERT INTO localdb.count(deviceid, count)
      VALUES ('${device}', ${count});`,
      function(error, results,fields) {
        resolve({"error":error, "results":results, "fields":fields});
      });

  })
}

exports.addAirquality = async function(device, temperature, humidity, co2){
  return new Promise((resolve) => {
    /*con.query(
      `INSERT INTO localdb.airquality(deviceid, temperature, humidity, CO2)
      VALUES (${device}, ${temperature}, ${huimdity}, ${co2});`,
      function(error, results,fields) {
        resolve({"error":error, "results":results, "fields":fields});
      });*/
      con.query(
        `INSERT INTO airquality(deviceid, temperature, humidity, CO2) VALUES ('${device}',${temperature},${humidity},${co2});`,
        function(error, results,fields) {
          resolve({"error":error, "results":results, "fields":fields});
        });
  })
}

exports.getDeviceAirquality = function(device){
  return new Promise((resolve) => {

    con.query(
      `SELECT temperature, humidity, CO2
      FROM localdb.airquality
      WHERE deviceID = '${device}'
      ORDER BY
        time DESC;`,
        function(error, results,fields) {
          resolve(results[0]);
        });

  })
}

exports.getDeviceCount = function(device){
  return new Promise((resolve) => {

    con.query(
      `SELECT SUM(count) AS count
      FROM count
      WHERE deviceID = '${device}';`,
        function(error, results,fields) {
          resolve(results[0]);
        });

  })
}

exports.removeDeviceFromRoom = function(deviceID){
  console.log("removeDevice", deviceID);
  return new Promise((resolve) => {
    con.query(
      `DELETE
      FROM localdb.roomdevice
      WHERE deviceID = '${deviceID}';`,function(error, result){
        console.log("roomdevice", error);
        if(!error){
          con.query(`
            DELETE
            FROM localdb.count
            WHERE deviceID = '${deviceID}';
          `, function(error, result){
              console.log("count", error);
              if(!error){
                con.query(`
                DELETE
                FROM localdb.airquality
                WHERE deviceID = '${deviceID}';
          `, function(error,result){
            resolve(result);
          })}
        })}
      })
  })
}

exports.getRooms = function(){
  return new Promise((resolve) => {
    con.query(
      `SELECT DISTINCT roomID
      FROM roomdevice
      WHERE 1;`,
      function(error, results, fields) {
        resolve(results);
      }
    );
  })
}
exports.getRoomCount = function(roomID){
  return new Promise((resolve) => {

      con.query(
        `SELECT SUM(count) as roomcount
        FROM localdb.count, localdb.roomdevice
        WHERE roomdevice.roomID = '${roomID}'
          AND roomdevice.deviceID = count.deviceID
          AND roomDevice.deviceType = 1;`,
          function(error, results, fields) {
            resolve(results[0]);
          }
      )
  })
}

exports.getAllRooms = function(roomID){
  return new Promise((resolve) => {
      con.query(
        `SELECT roomdevice.roomID as roomID, roomdevice.deviceID, roomdevice.deviceType
        FROM localdb.roomdevice
        WHERE roomdevice.roomID = '${roomID}'`,
          function(error, results, fields) {
            resolve(results);
          }
      )
  })
}
