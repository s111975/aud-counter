const port=process.env.PORT || 3000;

var express = require('express'),
  app = express(),
  bodyParser = require('body-parser')
  cors = require('cors');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use(express.static("template"));

const routes = require('./api/routes/routes.js');
routes(app);


app.listen(port);

console.log('server listening on port:', port);
