(function ($) {
  'use strict'
  var baseURL = "https://aud-counter.azurewebsites.net/";
  var interval = setInterval(starter, 20000);


  starter();
  function starter(){
    let $sidebar = $("#rooms-sidebar")
    $sidebar.html("");
    let $tables = $("#tables");
    $tables.html("");
    let $main = $("#main-content")
    $("#overview-link").click(function(){
      clearInterval(interval);
      let html = $("#overview");
      $('#main-content').html(html);
    });
    $main.html("");
    $.get(baseURL+'room/',
       async function(data, status, jqXHR){
        if(!$.isEmptyObject(data) ){
          var json = JSON.parse(data);
          for(var idx in json){
            var obj = json[idx];
            console.log("obj", obj);
            if(obj){
              addRow(obj["roomID"])
              addRoomToSidebar(obj["roomID"]);
              getRoomData(obj["roomID"]);
            }
            if(obj.hasOwnProperty("deviceType") && obj["deviceType"]=="COUNTER"){
              var id = obj["room"];
              var countObj = JSON.parse(await getRoomCount(id));
              if(countObj.hasOwnProperty("roomcount"))
                addCountBox(id, countObj["roomcount"]);
            }
            else if(obj.hasOwnProperty("deviceType") && obj["deviceType"]=="AIR"){
              var did = obj["deviceID"];
              var rid = obj["room"];
              var airObj = JSON.parse(await getRoomAir(did));
              if(airObj.hasOwnProperty("temperature"))
                addTempBox(rid, airObj["temperature"]);
              if(airObj.hasOwnProperty("humidity"))
                addHumBox(rid, airObj["humidity"]);
              if(airObj.hasOwnProperty("CO2"))
                addCOBox(rid, airObj["CO2"]);
            }
          }

        }
      })
  }

  async function getRoomData(id){
    addTable(id);
    console.log("getRoomData", id);
    var $roomTable = $("#table-body-"+id);

    $.get(baseURL+'room/'+id,
       async function(data, status, jqXHR){
           if(!$.isEmptyObject(data)){
           console.log("data", data);
           var json = JSON.parse(data);
           for(let idx in json){
             let obj = json[idx];
             console.log("obj", obj);
             if(obj && obj.hasOwnProperty("deviceType")){
               let deviceID = obj["deviceID"];
               $roomTable.append(`
                 <tr id=row-${obj["deviceID"]}>
                   <th scope="row">${parseInt(idx)+1}</th>
                   <td>${obj["roomID"]}</td>
                   <td>${obj["deviceID"]}</td>
                   <td>${obj["deviceType"]}</td>
                   <td><button id="remove-btn-${obj["deviceID"]}" type="button" class="btn btn-danger">remove</button></td>
                 </tr>
                 `)
                 let $btn = $("#remove-btn-"+deviceID);
                 $btn.on("click", function(){
                   console.log("#remove-btn-"+deviceID)
                   removeDevice(deviceID);
                 });
               if(obj.hasOwnProperty("deviceType") && obj["deviceType"]=="AIR"){
                 var did = obj["deviceID"];
                 var rid = id;
                 var airResponse = await getRoomAir(did);
                 if(!$.isEmptyObject(airResponse)){
                   var airObj = JSON.parse(airResponse);
                   if(airObj.hasOwnProperty("temperature"))
                     addTempBox(rid, airObj["temperature"]);
                   if(airObj.hasOwnProperty("humidity"))
                     addHumBox(rid, airObj["humidity"]);
                   if(airObj.hasOwnProperty("CO2"))
                     addCOBox(rid, airObj["CO2"]);
                 }
             }
           }
         }
       }
     });

      var countObj = JSON.parse(await getRoomCount(id));
      if(countObj.hasOwnProperty("roomcount"))
        addCountBox(id, countObj["roomcount"]);
    }

    function removeDevice(id){
      $.delete(baseURL+"room/", { "deviceID":id}, function(data){
        console.log("remove device data:", data);
        var json = JSON.parse(data);

        if(json.hasOwnProperty("error") && json["error"] == null){
          $('#post-response').html(
            `<div class="alert alert-success" role="alert">
              Device added!
            </div>`)
        }else if (json.hasOwnProperty("error") && json["error"] != null) {
          $('#post-response').html(
            `<div class="alert alert-danger" role="alert">
              ERROR: ${json["error"]["sqlMessage"]}
            </div>`)
        }else{
          console.log("did not hit if");
        }
        $("#row-"+id).remove();
      });
    }
    function addRow(id){
      var $container = $("#main-content");
      $container.append(`
        <div class="row" id=row-${id}></div>
        `)
    }
    function addRoomToSidebar(id){
      var $sidebar = $("#rooms-sidebar")
      $sidebar.append(`
      <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-circle"></i>
          <p>
            ${id}
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview" id="rooms-sidebar">
          <li class="nav-item">
            <a href="#" class="nav-link" name="overview-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Overview</p>
            </a>
          </li>
          <li class="nav-item">
            <a id=sidebar-devices-${id} href="#" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Devices
              </p>
            </a>
            </ul>
          </li>
        </ul>
      </li>`)
      $("#sidebar-devices-"+id).click(function(){
        $('#main-content').html($("#table-div-"+id).clone(true));
        console.log(interval);
        clearInterval(interval);
        console.log(interval);
      });
      $('[name ="overview-link"]').click(function(){
        clearInterval(interval);
        $('#main-content').html($("#overview").clone(true));

        showChart();
      });
    }

    function addDevicesToRoomMenu(id){

    }

    function addTable(id){
      console.log("addTable", id);
      var $tables = $("#tables")
      $tables.append(`<div id=table-div-${id}>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Room ID</th>
              <th scope="col">Device ID</th>
              <th scope="col">Device Type</th>
            </tr>
          </thead>
          <tbody id=table-body-${id}>
          </tbody>
        </table>
      </div>`)
    }
    function getRoomCount(roomID){
      return new Promise((resolve) => {
        $.get(baseURL+'room/'+roomID+'/count', function(data, status, jqXHR){
          resolve(data);
        })
      });
    }

    function getRoomAir(deviceID){
      return new Promise((resolve) => {
        $.get(baseURL+'device/airquality/'+deviceID+'/', function(data, status, jqXHR){
          console.log("getDeviceAir data", data, deviceID);
          resolve(data);
        })
      });
    }

    function addCountBox(roomID, count){
      var $row = $("#row-"+roomID);

      $row.append(`
        <div class="col-lg-3">
          <div class="small-box bg-info">
            <div class="inner">
              <h3>${count}</h3>
              <p>Currently in Auditorium ${roomID}</p>
            </div>
            <div class="icon">
              <i class="fas fa-bell"></i>
            </div>
          </div>
        </div>`
      );
    }

    function addTempBox(roomID, temp){
      var $row = $("#row-"+roomID);

      $row.append(`
        <div class="col-lg-3">
          <div class="small-box bg-info">
            <div class="inner">
              <h3>${temp}</h3>
              <p>Temperature in Auditorium ${roomID}</p>
            </div>
            <div class="icon">
              <i class="fas fa-thermometer-three-quarters "></i>
            </div>
          </div>
        </div>`
      );

    }

    function addHumBox(roomID, humidity){
      var $row = $("#row-"+roomID);

      $row.append(`
        <div class="col-lg-3">
          <div class="small-box bg-info">
            <div class="inner">
              <h3>${humidity}</h3>
              <p>Humidity in Auditorium ${roomID}</p>
            </div>
            <div class="icon">
              <i class="fas fa-shower"></i>
            </div>
          </div>
        </div>`
      );
    }

    function addCOBox(roomID, co2){
      var $row = $("#row-"+roomID);

      $row.append(`
        <div class="col-lg-3">
          <div class="small-box bg-info">
            <div class="inner">
              <h3>${co2}</h3>
              <p>CO in Auditorium ${roomID}</p>
            </div>
            <div class="icon">
              <i class="fas fa-cloud"></i>
            </div>
          </div>
        </div>`
      );

    }


    $("#addDevice").click(function(){
      clearInterval(interval);
      var html = $("#addDevicePage").html();
      $('#main-content').html(html);

      $('#register-device-form').click(function(){
        console.log("test");
        var roomID = $('#register-device #room-id').val();
        var deviceID = $('#register-device #device-id').val();
        var type = $('#register-device #device-type').val();
        $.post(baseURL+"room/", { "roomID":roomID, "deviceID":deviceID, "type":type}, function(data){
          var json = JSON.parse(data);
          if(json.hasOwnProperty("error") && json["error"] == null){
            $('#post-response').html(
              `<div class="alert alert-success" role="alert">
                Device added!
              </div>`)
          }else if (json.hasOwnProperty("error") && json["error"] != null) {
            $('#post-response').html(
              `<div class="alert alert-danger" role="alert">
                ERROR: ${json["error"]["sqlMessage"]}
              </div>`)
          }else{
            console.log("did not hit if");
          }
          console.log("data:", json);
        })
      });
    })

    $("#startpage").click(function(){
      $('#main-content').html("");
      clearInterval(interval);
      interval = setInterval(starter, 20000);
      starter();
    })



    $.delete = function(url, data, callback, type){

      if ( $.isFunction(data) ){
        type = type || callback,
            callback = data,
            data = {}
      }

      return $.ajax({
        url: url,
        type: 'DELETE',
        success: callback,
        data: data,
        contentType: type
      });
    }


  function showChart(){
    var counterChartCanvas = $('#countChart').get(0).getContext('2d')
    var airQChartCanvas = $('#airQChart').get(0).getContext('2d')
    var countChartData = {
      labels  : ['07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '09:30','12:00', '12:30','13:00', '13:30',],
      datasets: [
        {
          label               : 'Counter',
          backgroundColor     : 'rgba(210, 214, 222, 1)',
          borderColor         : 'rgba(210, 214, 222, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [0, 5, 36, 48, 49, 53, 55, 56, 59, 55, 32, 3, 34, 48, 57, 56, 56, 56, 56]
        }
      ]
    }

    var airQChartData = {
      labels  : ['07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '09:30','12:00', '12:30','13:00', '13:30',],
      datasets: [
        {
          label               : 'Temperature',
          backgroundColor     : 'rgba(210, 214, 222, 0.8)',
          borderColor         : 'rgba(210, 214, 222, 0.8)',
          pointRadius         : false,
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [20, 24, 28, 31, 32, 33, 34, 33, 34, 34, 28, 24, 25, 30, 31, 33, 34]
        },
        {
          label               : 'CO2',
          backgroundColor     : 'rgba(92, 140, 76, 0.8)',
          borderColor         : 'rgba(92, 140, 76, 0.8)',
          pointRadius         : false,
          pointColor          : 'rgba(92, 140, 76, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(148, 54, 76, 1)',
          data                : [20, 21, 23, 26, 27, 32, 35, 37, 40, 42, 37, 32, 34, 42, 44, 45,47, 45, 46]
        },
        {
          label               : 'Humidity',
          backgroundColor     : 'rgba(217, 152, 161, 1)',
          borderColor         : 'rgba(217, 152, 161, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(217, 152, 161, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(217, 152, 161,1)',
          data                : [30, 34, 36, 48, 49, 53, 55, 56, 59, 55, 43, 40, 45, 48, 57, 56, 56, 56, 56]
        }
      ]
    }

    var areaChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: true
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : false,
          }
        }]
      }
    }

    // This will get the first returned node in the jQuery collection.
    var countChart       = new Chart(counterChartCanvas, {
      type: 'line',
      data: countChartData,
      options: areaChartOptions
    })
    var airQChart       = new Chart(airQChartCanvas, {
      type: 'line',
      data: airQChartData,
      options: areaChartOptions
    })
  }

})(jQuery)
