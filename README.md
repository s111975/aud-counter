# Auditorium Counter

Backend server and web client for project in course: 34346. The project was to create a counting system and air quality measurement system for auditoriums.

## How to use the server

To install the server on your own system the following requirements and steps should be followed.

(this assumes the Gitlab repository is cloned, if working from a zipped archive of code, some steps can be omitted)
(some instructions also assumes working from a bash shell)
### Prerequisites

To use the server the following is required:

* MySQL server
* Node version 12 or higher
* NPM version 6 or higher
* An IP address that can be reached from the counting and measuring devices
* Git

### Installing

A step by step series of examples that tell you how to get a development env running

Clone the gitlab repository

```
git clone https://gitlab.gbar.dtu.dk/s111975/aud-counter.git
```

Change into directory

```
cd aud-counter
```

Install dependencies

```
npm install
```

Install DB

```
mysql -u USERNAME -p localdb < localdb.sql
```

change var baseURL in file aud-counter/template/dist/js/aud.js

```
var baseURL = "YOUR SERVER ADDRESS"
```

replace current connection in file aud-counter/api/services/dbservice.js

should follow the format of:

```
var con = mysql.createConnection({
  host     : IP address of mysql DB,
  port     : Port mysql DB,
  user     : DB connection username ,
  password : DB connection password,
  database : Name of database
});
```

## Deployment

You should now be able to start the server:
```
node app.js
```


## Built With

* [Node](https://nodejs.org/en/) - Programmed using node
* [NPM](https://www.npmjs.com/) - Node package manager, is automatically installed with Node
* [Express](https://expressjs.com/) - Nodejs server
* [MySQL](https://www.mysql.com/) - MySQL database
* [AdminLTE](https://adminlte.io/) - Template used for the client
